variable "vm_name" {
    description = "Name of the VM instance"
    type = string
    default = "Chrome-Desktop"
}

variable "machine_type" {
    description = "Type of machine to provision. Ex: e2-medium"
    type = string
    default = "e2-medium"
}

variable "zone" {
    description = "Zone name. Ex: a"
    type = string
}

variable "image_name" {
    description = "OS image to use. Ex: debian-cloud/debian-9 "
    type = string
    default = "debian-cloud/debian-9"
}

variable "network" {
    description = "Network to deploy in. Ex: my-VPC "
    type = string
    default = "default"
  
}