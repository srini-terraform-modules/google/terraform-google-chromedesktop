resource "google_compute_instance" "desktop" {
    name = var.vm_name
    machine_type = var.machine_type
    zone = var.zone

    boot_disk {
      initialize_params {
          image = var.image_name
      }
    }

    network_interface {
      network = var.network
      access_config {

      }
    }

    metadata_startup_script =  "./scripts/startup_script.sh"
}
